/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flip.sample.slice;

import com.yalantis.flip.sample.FriendProvider;
import com.yalantis.flip.sample.ResourceTable;
import com.yalantis.flip.sample.util.FlipItemUtil;
import com.yalantis.flip.sample.util.Utils;
import com.yalantis.flipviewpager.item.FlipItem;
import com.yalantis.flipviewpager.LayoutTouchListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;
import java.util.ArrayList;
import java.util.List;

/**
 * MainAbilitySlice
 *
 * @since 2021-07-15
 */
public class MainAbilitySlice extends AbilitySlice implements LayoutTouchListener {
    private final int num2 = 2;
    private final int offsetY = 100;
    private ListContainer friendListContainer;
    private List<List<FlipItem>> flipItemArray;
    private ToastDialog toastDialog;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        friendListContainer = (ListContainer) findComponentById(ResourceTable.Id_friendList);
        flipItemArray = new ArrayList<>();
        for (int i = 0; i < Utils.friends.size(); i += num2) {
            flipItemArray.add(FlipItemUtil.getFlipItems(i, i + 1, getContext()));
        }
        FriendProvider listProvider = new FriendProvider(flipItemArray, this);
        listProvider.setLayoutTouchListener(this);
        friendListContainer.setItemProvider(listProvider);
        friendListContainer.setLongClickable(false);
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean isEnable) {
        friendListContainer.setEnabled(isEnable);
    }

    @Override
    public void clickedItem(int position, int page, int direction) {
        // 点击friend头像时，显示nickname
        if (page == num2) {
            if (toastDialog == null) {
                toastDialog = new ToastDialog(this);
            }
            toastDialog
                    .setOffset(0,offsetY)
                    .setText(Utils.friends.get(position * num2 + direction).getNickname())
                    .show();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
