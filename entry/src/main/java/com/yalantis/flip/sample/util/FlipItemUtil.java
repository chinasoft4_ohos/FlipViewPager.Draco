/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flip.sample.util;

import com.yalantis.flip.sample.model.Friend;
import com.yalantis.flipviewpager.item.FlipItem;
import com.yalantis.flipviewpager.item.MyItem;
import com.yalantis.flipviewpager.item.MyText;
import ohos.agp.components.AttrHelper;
import ohos.agp.render.Paint;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import java.util.ArrayList;
import java.util.List;

/**
 * FlipItemUtil
 *
 * @since 2021-07-15
 */
public class FlipItemUtil {
    private static final int SCALE = 2;
    private static final int DEFAULT_TEXT_SIZE = 15;
    private static final int DEFAULT_NAME_SIZE = 30;
    private static final int DEFAULT_PADDING = 15;

    private FlipItemUtil() {
    }

    /**
     * getFlipItems
     *
     * @param left 第一个friend
     * @param right 第二个friend
     * @param context
     * @return FlipItem集合
     */
    public static List<FlipItem> getFlipItems(int left, int right, Context context) {
        Paint tempPaint = new Paint();
        int textSize = AttrHelper.fp2px(DEFAULT_TEXT_SIZE, context);
        int nameSize = AttrHelper.fp2px(DEFAULT_NAME_SIZE, context);
        int padding = AttrHelper.fp2px(DEFAULT_PADDING, context);
        tempPaint.setTextSize(textSize);
        Friend friendLeft = Utils.friends.get(left);
        Friend friendRight = Utils.friends.get(right);
        List<MyText> myTextsLeft = new ArrayList<>();
        for (String str : friendLeft.getInterests()) {
            float textWidth = tempPaint.measureText(str);
            myTextsLeft.add(new MyText(str, nameSize, textSize,
                    new RectFloat(0,0,textWidth + padding * SCALE,textSize + padding)));
        }
        List<MyText> myTextsRight = new ArrayList<>();
        for (String str : friendRight.getInterests()) {
            float textWidth = tempPaint.measureText(str);
            myTextsRight.add(new MyText(str, nameSize, textSize,
                    new RectFloat(0,0,textWidth + padding * SCALE,textSize + padding)));
        }
        tempPaint.setTextSize(nameSize);
        MyText leftName = new MyText(friendLeft.getNickname(), nameSize, textSize, new RectFloat(0,0,
                tempPaint.measureText(friendLeft.getNickname()) + padding * SCALE,textSize + padding));
        FlipItem flipItemLeft = new MyItem(leftName, myTextsLeft, friendLeft.getBackground());
        MyText rightName = new MyText(friendRight.getNickname(), nameSize, textSize, new RectFloat(0,0,
                tempPaint.measureText(friendRight.getNickname()) + padding * SCALE,textSize + padding));
        FlipItem flipItemRight = new MyItem(rightName, myTextsRight, friendRight.getBackground());
        PixelMap leftPixelMap = ResUtil.getPixelMap(context, friendLeft.getAvatar()).get();
        PixelMap rightPixelMap = ResUtil.getPixelMap(context, friendRight.getAvatar()).get();
        FlipItem flipItem = new MyItem(leftPixelMap, rightPixelMap);
        List<FlipItem> list = new ArrayList<>();
        list.add(flipItemLeft);
        list.add(flipItem);
        list.add(flipItemRight);
        return list;
    }
}
