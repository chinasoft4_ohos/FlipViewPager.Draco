package com.yalantis.flip.sample.util;

import com.yalantis.flip.sample.ResourceTable;
import com.yalantis.flip.sample.model.Friend;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yalantis
 */
public class Utils {
    public static final List<Friend> friends = new ArrayList<>();
    static {
        friends.add(new Friend(ResourceTable.Media_anastasia, "ANASTASIA", 0xFFEB6460, "Sport", "Literature", "Music", "Art", "Technology"));
        friends.add(new Friend(ResourceTable.Media_irene, "IRENE", 0xFFF4B63E, "Travelling", "Flights", "Books", "Painting", "Design"));
        friends.add(new Friend(ResourceTable.Media_kate, "KATE", 0xFF10b4a5, "Sales", "Pets", "Skiing", "Hairstyles", "Сoffee"));
        friends.add(new Friend(ResourceTable.Media_paul, "PAUL", 0xFFff4284, "Android", "Development", "Design", "Wearables", "Pets"));
        friends.add(new Friend(ResourceTable.Media_daria, "DARIA", 0xFFff8c6a, "Design", "Fitness", "Healthcare", "UI/UX", "Chatting"));
        friends.add(new Friend(ResourceTable.Media_kirill, "KIRILL", 0xFFF4B63E, "Development", "Android", "Healthcare", "Sport", "Rock Music"));
        friends.add(new Friend(ResourceTable.Media_julia, "JULIA", 0xFF10b4a5, "Cinema", "Music", "Tatoo", "Animals", "Management"));
        friends.add(new Friend(ResourceTable.Media_yalantis, "YALANTIS", 0xFF7c57e4, "Android", "IOS", "Application", "Development", "Company"));
    }
}
