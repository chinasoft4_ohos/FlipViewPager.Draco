/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flip.sample;

import com.yalantis.flipviewpager.item.FlipItem;
import com.yalantis.flipviewpager.FlipViewPager;
import com.yalantis.flipviewpager.LayoutTouchListener;
import com.yalantis.flipviewpager.SimpleFlipShading;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

/**
 * FriendProvider
 *
 * @since 2021-07-15
 */
public class FriendProvider extends BaseItemProvider {
    private List<List<FlipItem>> flipItemArray;
    private Context context;
    private LayoutTouchListener layoutTouchListener;

    /**
     * 构造器
     *
     * @param flipItemArray
     * @param context
     */
    public FriendProvider(List<List<FlipItem>> flipItemArray, Context context) {
        this.flipItemArray = flipItemArray;
        this.context = context;
    }

    /**
     * 设置touch监听
     *
     * @param layoutTouchListener
     */
    public void setLayoutTouchListener(LayoutTouchListener layoutTouchListener) {
        this.layoutTouchListener = layoutTouchListener;
    }

    @Override
    public int getCount() {
        return flipItemArray.size();
    }

    @Override
    public Object getItem(int i) {
        return flipItemArray.get(i);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt = component;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_friend_item, null, false);
        }
        final int defaultPage = 2;
        FlipViewPager flipViewPager = (FlipViewPager) cpt.findComponentById(ResourceTable.Id_flip_list);
        flipViewPager.setIndex(position);
        flipViewPager.setLayoutTouchListener(this.layoutTouchListener);
        flipViewPager.setFoldShading(new SimpleFlipShading());
        flipViewPager.setProvider(new FlipViewPager.FlipProvider(flipItemArray.get(position)));
        flipViewPager.setCurrentPage(defaultPage);
        return cpt;
    }
}
