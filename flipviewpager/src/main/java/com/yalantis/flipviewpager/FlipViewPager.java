/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager;

import com.yalantis.flipviewpager.item.FlipItem;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;
import java.util.List;

/**
 * FlipViewPager
 *
 * @since 2021-07-15
 */
public class FlipViewPager extends Component implements Component.DrawTask, Component.TouchEventListener {
    private static final long ANIMATION_DURATION_PER_ITEM = 200L;
    private static final int NUM2 = 2;
    private static final float MIN = 0.1f;
    private static final int TRIGGER_OFFSET_X = 8;
    private static final int TRIGGER_OFFSET_Y = 5;
    private static final int INFINITE = -1;
    private static final int DEFAULT_HALF_PAGE_ROTATION = 90;
    private static final int DEFAULT_PAGE_ROTATION = 180;
    private static final int DEFAULT_SPEED = 1000;

    private int index;
    private SimpleFlipShading flipShading;
    private FlipProvider provider;
    private LayoutTouchListener layoutTouchListener;
    private AnimatorValue animator;

    private float foldRotation;
    private float minRotation;
    private float maxRotation;
    private float touchX;
    private float touchY;
    private float screenTouchY;

    private long lastTime;
    private int mSpeed;
    private float pageRotation;

    private boolean isCheckTouch = false;
    private boolean isDragging = false;

    /**
     * FlipViewPager
     *
     * @param context
     */
    public FlipViewPager(Context context) {
        this(context, null);
    }

    /**
     * FlipViewPager
     *
     * @param context
     * @param attrSet
     */
    public FlipViewPager(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * FlipViewPager
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public FlipViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private void init() {
        animator = new AnimatorValue();
        addDrawTask(this);
        setTouchEventListener(this);
    }

    public void setLayoutTouchListener(LayoutTouchListener listener) {
        layoutTouchListener = listener;
    }

    /**
     * setFoldShading
     *
     * @param shading
     */
    public void setFoldShading(SimpleFlipShading shading) {
        flipShading = shading;
        invalidate();
    }

    /**
     * setProvider
     *
     * @param provider
     */
    public void setProvider(FlipProvider provider) {
        if (provider != null && provider.flipItems != null) {
            this.provider = provider;
            minRotation = 0;
            maxRotation = DEFAULT_PAGE_ROTATION * (provider.flipItems.size() - 1);
            invalidate();
        }
    }

    private float getFoldRotation() {
        return foldRotation;
    }

    /**
     * setCurrentPage
     *
     * @param page
     */
    public void setCurrentPage(int page) {
        if (page > 0 && page <= provider.flipItems.size()) {
            setFoldRotation((page - 1) * DEFAULT_PAGE_ROTATION, true);
        }
    }

    private void setFoldRotation(float rotation, boolean isFromUser) {
        if (isFromUser) {
            animator.cancel();
        }
        foldRotation = Math.min(Math.max(minRotation, rotation), maxRotation);
        invalidate();
    }

    private void animateFold(float to) {
        animateFold(to, DEFAULT_PAGE_ROTATION);
    }

    private void animateFold(float to, int speed) {
        final float from = getFoldRotation();
        final long duration = (long) Math.abs(ANIMATION_DURATION_PER_ITEM * (to - from) / speed);
        animator.stop();
        animator.cancel();
        animator.setValueUpdateListener((animatorValue, value) -> {
            foldRotation = from + (to - from) * value;
            invalidate();
        });
        animator.setDuration(duration);
        animator.start();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        Size size = new Size(getWidth() - getPaddingLeft() - getPaddingRight(),
                getHeight() - getPaddingTop() - getPaddingBottom());
        if (!(provider != null && provider.flipItems != null && provider.flipItems.size() > 0)) {
            return;
        }
        int[]background = {INFINITE, INFINITE};
        int[]threeDim = {INFINITE, INFINITE};
        int[]foreground = {INFINITE, INFINITE};
        for (int i = 0; i < provider.flipItems.size(); i++) {
            if (foldRotation > i * DEFAULT_PAGE_ROTATION - DEFAULT_PAGE_ROTATION
                    && foldRotation < i * DEFAULT_PAGE_ROTATION + DEFAULT_PAGE_ROTATION) {
                if (foldRotation < i * DEFAULT_PAGE_ROTATION) {
                    if (about(foldRotation, i * DEFAULT_PAGE_ROTATION - DEFAULT_HALF_PAGE_ROTATION)) {
                        provider.flipItems.get(i).draw(canvas, size, 0, LayoutAlignment.BOTTOM, flipShading);
                        canvas.drawLine(0, size.height * 1.0F / NUM2, size.width,
                                size.height * 1.0F / NUM2, new Paint(), Color.BLACK);
                    } else if (foldRotation > i * DEFAULT_PAGE_ROTATION - DEFAULT_HALF_PAGE_ROTATION) {
                        threeDim[0] = i;
                        threeDim[1] = LayoutAlignment.TOP;
                        foreground[0] = i;
                        foreground[1] = LayoutAlignment.BOTTOM;
                    } else {
                        background[0] = i;
                        background[1] = LayoutAlignment.BOTTOM;
                    }
                } else if (foldRotation > i * DEFAULT_PAGE_ROTATION) {
                    if (about(foldRotation, i * DEFAULT_PAGE_ROTATION + DEFAULT_HALF_PAGE_ROTATION)) {
                        provider.flipItems.get(i).draw(canvas, size, 0, LayoutAlignment.TOP, flipShading);
                        canvas.drawLine(0, size.height * 1.0F / NUM2, size.width,
                                size.height * 1.0F / NUM2, new Paint(), Color.BLACK);
                    } else if (foldRotation < i * DEFAULT_PAGE_ROTATION + DEFAULT_HALF_PAGE_ROTATION) {
                        threeDim[0] = i;
                        threeDim[1] = LayoutAlignment.BOTTOM;
                        foreground[0] = i;
                        foreground[1] = LayoutAlignment.TOP;
                    } else {
                        background[0] = i;
                        background[1] = LayoutAlignment.TOP;
                    }
                } else {
                    provider.flipItems.get(i).onDraw(canvas, size.width, size.height);
                }
            }
        }
        drawGroundFlip(canvas, size, background);
        drawThreeDimFlip(canvas, size, threeDim);
        drawGroundFlip(canvas, size, foreground);
    }

    private void drawGroundFlip(Canvas canvas, Size size, int[]ground) {
        if (ground[0] != INFINITE && ground[1] != INFINITE
                && ground[0] < provider.flipItems.size()) {
            provider.flipItems.get(ground[0]).draw(canvas, size, 0, ground[1], flipShading);
        }
    }

    private void drawThreeDimFlip(Canvas canvas, Size size, int[]threeDim) {
        if (threeDim[0] != INFINITE && threeDim[1] != INFINITE
                && threeDim[0] < provider.flipItems.size()) {
            provider.flipItems.get(threeDim[0]).draw(canvas, size, foldRotation - threeDim[0] * DEFAULT_PAGE_ROTATION,
                    threeDim[1], flipShading);
        }
    }

    private boolean about(float a, float b) {
        if (Math.abs(a - b) < MIN) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (animator != null) {
            animator.stop();
        }
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            touchDown(touchEvent);
        } else if (touchEvent.getAction() == TouchEvent.POINT_MOVE) {
            int type = move(touchEvent);
            if (type != INFINITE) {
                return type == 1;
            }
        } else if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP
                || touchEvent.getAction() == TouchEvent.CANCEL) {
            touchUp(touchEvent, component);
        }
        return true;
    }

    private void touchDown(TouchEvent touchEvent) {
        touchX = getTouchX(touchEvent, 0);
        touchY = getTouchY(touchEvent, 0);
        screenTouchY = touchEvent.getPointerScreenPosition(0).getY();
        mSpeed = 0;
        pageRotation = 0;
        lastTime = System.currentTimeMillis();
        isCheckTouch = false;
        isDragging = false;
    }

    private int move(TouchEvent touchEvent) {
        float offsetX = getTouchX(touchEvent, 0) - touchX;
        float offsetY = getTouchY(touchEvent, 0) - touchY;
        if (!isDragging && isCheckTouch) {
            return 0;
        }
        if (!isCheckTouch) {
            if (Math.abs(offsetY) > Math.abs(offsetX)) {
                disallowParentInterceptTouchEvent(true);
                isDragging = false;
                isCheckTouch = true;
                return 0;
            } else if (Math.abs(offsetX) > TRIGGER_OFFSET_X) {
                disallowParentInterceptTouchEvent(false);
                touchX = getTouchX(touchEvent, 0);
                touchY = getTouchY(touchEvent, 0);
                isDragging = true;
                isCheckTouch = true;
            }
            return 1;
        }
        float rotation = -DEFAULT_PAGE_ROTATION * offsetX * NUM2 / getWidth();
        if (pageRotation + rotation <= DEFAULT_PAGE_ROTATION && pageRotation + rotation >= -DEFAULT_PAGE_ROTATION) {
            pageRotation += rotation;
        } else if (pageRotation + rotation > DEFAULT_PAGE_ROTATION) {
            rotation = DEFAULT_PAGE_ROTATION - pageRotation;
            pageRotation = DEFAULT_PAGE_ROTATION;
        } else if (pageRotation + rotation < -DEFAULT_PAGE_ROTATION) {
            rotation = -DEFAULT_PAGE_ROTATION - pageRotation;
            pageRotation = -DEFAULT_PAGE_ROTATION;
        }
        setFoldRotation(foldRotation + rotation, true);
        if (lastTime != 0) {
            mSpeed = (int) (rotation * DEFAULT_SPEED / (System.currentTimeMillis() - lastTime));
        }
        lastTime = System.currentTimeMillis();
        touchX = getTouchX(touchEvent, 0);
        touchY = getTouchY(touchEvent, 0);

        return INFINITE;
    }

    private void touchUp(TouchEvent touchEvent, Component component) {
        int from = (int) foldRotation;
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP && !isDragging) {
            if (System.currentTimeMillis() - lastTime < ANIMATION_DURATION_PER_ITEM
                && Math.abs(touchEvent.getPointerScreenPosition(0).getY() - screenTouchY) < TRIGGER_OFFSET_Y) {
                clickedItem(touchX < component.getWidth() * 1.0F / NUM2 ? 0 : 1, from / DEFAULT_PAGE_ROTATION + 1);
                return;
            }
        }
        if (mSpeed > DEFAULT_PAGE_ROTATION || mSpeed < -DEFAULT_PAGE_ROTATION) {
            int to;
            if (mSpeed > 0) {
                to = getNextRotation(foldRotation);
            } else {
                to = getLastRotation(foldRotation);
            }
            animateFold(to, mSpeed);
        } else {
            int to;
            if (Math.abs(getNextRotation(foldRotation) - from)
                    < Math.abs(getLastRotation(foldRotation) - from)) {
                to = getNextRotation(foldRotation);
            } else {
                to = getLastRotation(foldRotation);
            }
            animateFold(to);
        }
        if (isDragging) {
            disallowParentInterceptTouchEvent(false);
        }
    }

    private void clickedItem(int direction, int page) {
        if (layoutTouchListener != null) {
            layoutTouchListener.clickedItem(index, page, direction);
        }
    }

    private void disallowParentInterceptTouchEvent(boolean isEnable) {
        if (layoutTouchListener != null) {
            layoutTouchListener.requestDisallowInterceptTouchEvent(isEnable);
        }
    }

    /**
     * FlipProvider
     *
     * @since 2021-07-15
     */
    public static class FlipProvider {
        private List<FlipItem> flipItems;

        /**
         * FlipProvider
         *
         * @param flipItems
         */
        public FlipProvider(List<FlipItem> flipItems) {
            this.flipItems = flipItems;
        }

        public int getCount() {
            return flipItems == null ? 0 : flipItems.size();
        }
    }

    private int getLastRotation(float rotation) {
        return (int) rotation / DEFAULT_PAGE_ROTATION * DEFAULT_PAGE_ROTATION;
    }

    private int getNextRotation(float rotation) {
        if (getLastRotation(rotation) == (int) rotation) {
            return getLastRotation(rotation);
        }
        return (int) rotation / DEFAULT_PAGE_ROTATION * DEFAULT_PAGE_ROTATION + DEFAULT_PAGE_ROTATION;
    }

    private float getTouchX(TouchEvent touchEvent, int currentIndex) {
        float tempTouchX = 0;
        if (touchEvent.getPointerCount() > currentIndex) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == NUM2) {
                tempTouchX = touchEvent.getPointerScreenPosition(currentIndex).getX() - xy[0];
            } else {
                tempTouchX = touchEvent.getPointerPosition(currentIndex).getX();
            }
        }
        return tempTouchX;
    }

    private float getTouchY(TouchEvent touchEvent, int currentIndex) {
        float tempTouchY = 0;
        if (touchEvent.getPointerCount() > currentIndex) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == NUM2) {
                tempTouchY = touchEvent.getPointerScreenPosition(currentIndex).getY() - xy[1];
            } else {
                tempTouchY = touchEvent.getPointerPosition(currentIndex).getY();
            }
        }
        return tempTouchY;
    }
}
