/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager.item;

import com.yalantis.flipviewpager.SimpleFlipShading;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;
import ohos.media.image.common.Size;

/**
 * FlipItem
 *
 * @since 2021-07-15
 */
public abstract class FlipItem {
    private static final int NUM2 = 2;
    private Paint paint = new Paint();

    /**
     * onDraw
     *
     * @param canvas
     * @param width
     * @param height
     */
    public abstract void onDraw(Canvas canvas, int width, int height);

    /**
     * 绘制 3D效果
     *
     * @param canvas
     * @param size
     * @param rotation
     * @param gravity
     * @param flipShading
     */
    public void draw(Canvas canvas, Size size, float rotation, int gravity, SimpleFlipShading flipShading) {
        int saveCount = canvas.save();
        double sin = Math.abs(Math.sin(Math.toRadians(rotation)));
        float dh = (float) (size.width * sin);
        float scaleFactor = size.height / (size.height + dh);
        if (gravity == LayoutAlignment.TOP) {
            if (rotation != 0) {
                rotation(canvas, size, rotation);
            }
            RectFloat topPart = new RectFloat(0, 0, size.width * 1.0F / NUM2, size.height);
            if (rotation == 0) {
                canvas.saveLayer(topPart, paint);
            } else {
                canvas.scale(scaleFactor, 1, size.width * 1.0F / NUM2, size.height * 1.0F / NUM2);
            }
            onDraw(canvas, size.width, size.height);
            if (flipShading != null) {
                flipShading.onDraw(canvas, topPart, rotation, gravity);
            }
        } else if (gravity == LayoutAlignment.BOTTOM) {
            if (rotation != 0) {
                rotation(canvas, size, rotation);
            }
            RectFloat bottomPart = new RectFloat(size.width * 1.0F / NUM2, 0, size.width, size.height);
            if (rotation == 0) {
                canvas.saveLayer(bottomPart, paint);
            } else {
                canvas.scale(scaleFactor, 1, size.width * 1.0F / NUM2, size.height * 1.0F / NUM2);
            }
            onDraw(canvas, size.width, size.height);
            if (flipShading != null) {
                flipShading.onDraw(canvas, bottomPart, rotation, gravity);
            }
        }
        canvas.restoreToCount(saveCount);
    }

    private void rotation(Canvas canvas, Size size, float rotation) {
        ThreeDimView threeDimView = new ThreeDimView();
        Matrix matrix = new Matrix();
        threeDimView.rotateY(-rotation);
        threeDimView.getMatrix(matrix);
        matrix.preTranslate(-size.width * 1.0F / NUM2, -size.height * 1.0F / NUM2);
        matrix.postTranslate(size.width * 1.0F / NUM2, size.height * 1.0F / NUM2);
        canvas.concat(matrix);
    }
}
