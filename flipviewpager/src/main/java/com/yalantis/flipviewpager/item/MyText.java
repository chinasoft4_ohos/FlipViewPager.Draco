/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager.item;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;

/**
 * 文字信息
 *
 * @since 2021-07-15
 */
public class MyText {
    private static final float RATE = 2.0f / 3;
    private static final int NUM2 = 2;
    private RectFloat rectFloat;
    private String text;
    private float endX;
    private int textSize;
    private int nameSize;

    /**
     * 构造器
     *
     * @param text
     * @param nameSize
     * @param textSize
     * @param rectFloat
     */
    public MyText(String text, int nameSize, int textSize, RectFloat rectFloat) {
        this.text = text;
        this.rectFloat = rectFloat;
        this.textSize = textSize;
        this.nameSize = nameSize;
    }

    /**
     * getEndX
     *
     * @return endX
     */
    public float getEndX() {
        return endX;
    }

    public float getHeight() {
        return rectFloat.getHeight();
    }

    /**
     * 绘制名字
     *
     * @param canvas
     * @param paint
     * @param offsetX
     * @param offsetY
     */
    public void drawName(Canvas canvas, Paint paint, float offsetX, float offsetY) {
        canvas.save();
        canvas.translate(offsetX, offsetY);
        paint.setTextSize(nameSize);
        paint.setTextAlign(TextAlignment.LEFT);
        paint.setColor(Color.WHITE);
        canvas.drawText(paint, text, 0, rectFloat.getHeight() * RATE);
        canvas.restore();
    }

    /**
     * 绘制文字
     *
     * @param canvas
     * @param paint
     * @param offsetX
     * @param offsetY
     */
    public void draw(Canvas canvas, Paint paint, float offsetX, float offsetY) {
        canvas.save();
        canvas.translate(offsetX, offsetY);
        paint.setTextSize(textSize);
        paint.setTextAlign(TextAlignment.CENTER);
        paint.setColor(Color.DKGRAY);
        canvas.drawRoundRect(rectFloat, rectFloat.getHeight() / NUM2, rectFloat.getHeight() / NUM2, paint);
        paint.setColor(Color.WHITE);
        canvas.drawText(paint, text, rectFloat.getCenter().getPointX(), rectFloat.getHeight() * RATE);
        this.endX = offsetX + rectFloat.getWidth();
        canvas.restore();
    }
}
