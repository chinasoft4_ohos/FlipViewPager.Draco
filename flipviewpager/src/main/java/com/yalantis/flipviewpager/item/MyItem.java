/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager.item;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.media.image.PixelMap;

import java.util.List;

/**
 * 滑动效果 Item，需要实现onDraw方法
 *
 * @since 2021-07-15
 */
public class MyItem extends FlipItem {
    private static final int NUM2 = 2;
    private static final int COL = 3;
    private static final int OFFSET_X = 75;
    private static final int OFFSET_Y = 50;
    private static final int PADDING = 25;
    private PixelMap left;
    private PixelMap right;
    private List<MyText> myTextList;
    private Paint paint;
    private Color color;
    private MyText name;

    /**
     * 构造方法
     *
     * @param left 左侧 PixelMap
     * @param right 右侧 PixelMap
     */
    public MyItem(PixelMap left, PixelMap right) {
        this.left = left;
        this.right = right;
        init();
    }

    /**
     * 构造器
     *
     * @param name 姓名text
     * @param myTextList 兴趣 text list
     * @param color
     */
    public MyItem(MyText name, List<MyText> myTextList, int color) {
        this.myTextList = myTextList;
        this.name = name;
        this.color = new Color(color);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setTextAlign(TextAlignment.CENTER);
    }

    @Override
    public void onDraw(Canvas canvas, int width, int height) {
        if (color != null) {
            paint.setColor(this.color);
        }
        if (left != null && right != null) {
            canvas.drawPixelMapHolderRect(new PixelMapHolder(left),
                    new RectFloat(0, 0, width * 1.0F / NUM2, height), paint);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(right),
                    new RectFloat(width * 1.0F / NUM2, 0, width, height), paint);
        } else {
            canvas.drawRect(0,0, width, height, paint);
        }

        if (myTextList != null) {
            canvas.drawRect(0,0, width, height, paint);
            name.drawName(canvas, paint, OFFSET_X, name.getHeight());
            MyText tempText;
            for (int i = 0; i < myTextList.size(); i++) {
                tempText = myTextList.get(i);
                if (i < COL) {
                    tempText.draw(canvas, paint, i % COL == 0 ? OFFSET_X : PADDING + myTextList.get(i - 1).getEndX(),
                            height * 1.0F / NUM2 - tempText.getHeight() * 1.0F / NUM2);
                } else {
                    tempText.draw(canvas, paint, i % COL == 0 ? OFFSET_X : PADDING + myTextList.get(i - 1).getEndX(),
                            OFFSET_Y + height * 1.0F / NUM2 + tempText.getHeight() * 1.0F / NUM2);
                }
            }
        }
    }
}
