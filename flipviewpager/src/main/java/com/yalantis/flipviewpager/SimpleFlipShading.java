/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.RectFloat;

/**
 * SimpleFlipShading
 *
 * @since 2021-07-15
 */
public class SimpleFlipShading {
    private static final Color SHADOW_COLOR_BOTTOM = Color.WHITE;
    private static final Color SHADOW_COLOR_TOP = Color.BLACK;
    private static final int SHADOW_MAX_ALPHA = 130;
    private static final float MAX_ALPHA = 255f;
    private static final int ANGLE = 90;
    private final Paint solidShadow;

    /**
     * 构造器
     */
    public SimpleFlipShading() {
        solidShadow = new Paint();
    }

    /**
     * onDraw
     *
     * @param canvas
     * @param bounds
     * @param rotation
     * @param gravity
     */
    public void onDraw(Canvas canvas, RectFloat bounds, float rotation, int gravity) {
        float intensity = getShadowIntensity(rotation, gravity);
        if (intensity > 0f) {
            if (gravity == LayoutAlignment.TOP) {
                solidShadow.setColor(SHADOW_COLOR_TOP);
            } else {
                solidShadow.setColor(SHADOW_COLOR_BOTTOM);
            }
            float alpha = SHADOW_MAX_ALPHA * intensity / MAX_ALPHA;
            solidShadow.setAlpha(alpha);
            canvas.drawRect(bounds, solidShadow);
        }
    }

    private float getShadowIntensity(float rotation, int gravity) {
        float intensity = 0f;
        if (gravity == LayoutAlignment.TOP) {
            if (rotation > -ANGLE && rotation < 0f) {
                intensity = -rotation / ANGLE;
            }
        } else {
            if (rotation > 0f && rotation < ANGLE) {
                intensity = rotation / ANGLE;
            }
        }
        return intensity;
    }
}
