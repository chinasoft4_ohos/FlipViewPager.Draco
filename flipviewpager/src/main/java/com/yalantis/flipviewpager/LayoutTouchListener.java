/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.flipviewpager;

/**
 * 滑动监听
 *
 * @since 2021-07-15
 */
public interface LayoutTouchListener {
    /**
     * 接口TouchEvent
     *
     * @param isEnable
     */
    void requestDisallowInterceptTouchEvent(boolean isEnable);

    /**
     * 点击某个item的事件回调
     *
     * @param position
     * @param page
     * @param direction
     */
    void clickedItem(int position, int page, int direction);
}
