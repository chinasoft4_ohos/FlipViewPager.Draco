# FlipViewPager.Draco

####  项目介绍
- 项目名称：FlipViewPager.Draco
- 所属系列：openharmony的第三方组件适配移植
- 功能：openharmony 这个项目提供一个页面3D翻转实功能，在ListContainer中使用。在item上左右滑动，可以实现页面的切换。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本： master分支



####  效果演示
<p align="center">
  <img src="image/demo.gif" alt="FlipViewPager.Draco"  />
</p>


#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
```xml
 dependencies {
    implementation('com.gitee.chinasoft_ohos:FlipViewPager.Draco:0.0.3-SNAPSHOT')
    
 }
```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

xml布局引用 FlipViewPager

```xml
<?xml version="1.0" encoding="utf-8"?>
<com.yalantis.flipviewpager.FlipViewPager
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:id="$+id:flip_list"
    ohos:height="200vp"
    ohos:width="match_parent"/>
```
 用自己的Provider继承 BaseFlipAdapter，实现抽象的方法

```java
public class FriendProvider extends BaseItemProvider {
    // ... other code
    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt = component;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_friend_item, null, false);
        }
        final int defaultPage = 2;
        FlipViewPager flipViewPager = (FlipViewPager) cpt.findComponentById(ResourceTable.Id_flip_list);
        //设置flipView的位置
        flipViewPager.setIndex(position);
        //设置touch监听
        flipViewPager.setLayoutTouchListener(this.layoutTouchListener);
        //设置页面时阴影效果
        flipViewPager.setFoldShading(new SimpleFlipShading());
        //设置provider
        flipViewPager.setProvider(new FlipViewPager.FlipProvider(flipItemArray.get(position)));
        //设置默认页
        flipViewPager.setCurrentPage(defaultPage);
        return cpt;
    }
}
```
 实现LayoutTouchListener监听
```java
public class MainAbilitySlice extends AbilitySlice implements LayoutTouchListener {
    // other code
    @Override
    public void requestDisallowInterceptTouchEvent(boolean isEnable) {
        //父布局是否响应事件，用来处理滑动冲突
        friendListContainer.setEnabled(isEnable);
    }

    @Override
    public void clickedItem(int position, int page, int direction) {
        // 点击friend头像时，显示nickname
        if (page == num2) {
            new ToastDialog(this)
                    .setOffset(0,offsetY)
                    .setText(Utils.friends.get(position * num2 + direction).getNickname())
                    .show();
        }
    }
}

```
 设置你的Provider ```ListContainer```

```Java
public class MainAbilitySlice extends AbilitySlice implements LayoutTouchListener {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        friendListContainer = (ListContainer) findComponentById(ResourceTable.Id_friendList);
        flipItemArray = new ArrayList<>();
        for (int i = 0; i < Utils.friends.size(); i += num2) {
            flipItemArray.add(FlipItemUtil.getFlipItems(i, i + 1, getContext()));
        }
        FriendProvider listProvider = new FriendProvider(flipItemArray, this);
        // 设置监听
        listProvider.setLayoutTouchListener(this);
        // 设置内容提供者
        friendListContainer.setItemProvider(listProvider);
        friendListContainer.setLongClickable(false);
    }
}

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

0.0.3-SNAPSHOT

#### 版权和许可信息
```
Copyright 2015, Yalantis

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
